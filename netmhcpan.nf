#!/usr/bin/env nextflow

process netmhcpan {
// Predicts binding of peptides to any MHC molecule of known sequence using
// artificial neural networks (ANNs).
//
// input:
// output:

// require:
//   PEPTIDES_AND_ALLELES

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'netmhcpan_container'
  label 'netmhcpan'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/netmhcpan"
  cache 'lenient'

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(peptides), path(alleles)
  val parstr

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*.fa.netmhcpan.txt"), emit: c1_antis

  script:
  """
  export TMPDIR="\${PWD}"
  /netMHCpan-4.1/Linux_x86_64/bin/netMHCpan -listMHC > allowed_alleles

  for i in `cat *netmhcpan.alleles* | sed 's/,/\\n/g'`; do echo \${i}; grep "^\${i}\$" allowed_alleles | cat >> final_alleles; done

  EXPLICIT_ALLELES=\$(cat final_alleles | sed -z 's/\\n/\\t/g ' | sed 's/\\t\$//g')
  PEPTIDES_FILENAME="${peptides}"

  grep -v "^;" ${peptides} > peptides.no_comms.fa #Apparently netmhcpan can see commented lines.

  # Splitting up by number of CPUs provided. Have to do this in a bit of a hacky way to ensure header and sequence stay together.
  sed -z 's/\\n/%/g' peptides.no_comms.fa | sed -z 's/%>/\\n>/g' > peptides.no_comms.one_line.fa
  split_count=\$((${task.cpus}/6)) #Assuming six class I alleles detected.
  line_count=\$(wc -l peptides.no_comms.one_line.fa | cut -f 1 -d ' ')

  if [ \${line_count} -lt \${split_count} ]; then
    cp peptides.no_comms.one_line.fa xaa
  else
    per_file_count=\$((\${line_count}/\${split_count}))
    split -l \${per_file_count} peptides.no_comms.one_line.fa
  fi

  for i in xa[a-z]; do
    echo \${i}
    sed -z 's/%/\\n/g' \${i} > \${i}.fixt
    rm \${i}
  done


  if [[ -s ${peptides} ]]; then
    export TMPDIR="\${PWD}"
    for allele in \${EXPLICIT_ALLELES}; do
      for inp in x*fixt; do
        /netMHCpan-4.1/Linux_x86_64/bin/netMHCpan -BA -s -a \${allele} -inptype 0 -f \${inp} > \${inp}.\${allele}.netmhcpan.txt&
      done
    done
  else
    PEPTIDES_FILENAME="${peptides}"
    echo "NO PEPTIDES" > \${PEPTIDES_FILENAME%.aa.fa}.netmhcpan.txt
  fi
  wait

  for split in x*txt; do
    cat \${split} >> \${PEPTIDES_FILENAME%.aa.fa}.netmhcpan.txt
  done
  """
}

process netmhcpan_rna {
// Predicts binding of peptides to any MHC molecule of known sequence using
// artificial neural networks (ANNs).
//
// input:
// output:

// require:
//   PEPTIDES_AND_ALLELES

  tag "${dataset}/${pat_name}/${run}"
  label 'netmhcpan_container'
  label 'netmhcpan'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/netmhcpan"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(peptides), path(alleles)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*netmhcpan*"), emit: c1_antis

  script:
  """
  export TMPDIR="\${PWD}"
  /netMHCpan-4.1/Linux_x86_64/bin/netMHCpan -listMHC > allowed_alleles

  for i in `cat *netmhcpan.alleles* | sed 's/,/\\n/g'`; do echo \${i}; grep "^\${i}\$" allowed_alleles | cat >> final_alleles; done

  EXPLICIT_ALLELES=\$(cat final_alleles | sed -z 's/\\n/\\t/g ' | sed 's/\\t\$//g')
  PEPTIDES_FILENAME="${peptides}"

  grep -v "^;" ${peptides} > peptides.no_comms.fa #Apparent netmhcpan can see commented lines.

  if [[ -s ${peptides} ]]; then
    export TMPDIR="\${PWD}"
    for allele in \${EXPLICIT_ALLELES}; do
      /netMHCpan-4.1/Linux_x86_64/bin/netMHCpan -BA -s -a \${allele} -inptype 0 -f peptides.no_comms.fa > \${PEPTIDES_FILENAME%.aa.fa}.\${allele}.netmhcpan.txt&
    done
  else
    PEPTIDES_FILENAME="${peptides}"
    echo "NO PEPTIDES" > \${PEPTIDES_FILENAME%.aa.fa}.netmhcpan.txt
  fi
  wait

  for allele in \${EXPLICIT_ALLELES}; do
    cat \${PEPTIDES_FILENAME%.aa.fa}.\${allele}.netmhcpan.txt >> \${PEPTIDES_FILENAME%.aa.fa}.netmhcpan.txt
  done
  """
}
